

# EC2 Launch Configuration to create EC2 instances that run the ECS agent, then ensure they are registered into the ECS cluster
resource "aws_launch_configuration" "cinc-cluster" {
  name_prefix                 = "${var.servicename}-${var.environment}-"
  image_id                    = data.aws_ami.ubuntu.id
  instance_type               = var.ec2_instance_type
  key_name                    = aws_key_pair.accesskey.key_name
  security_groups             = [aws_security_group.cinc-cluster.id]
  iam_instance_profile        = aws_iam_instance_profile.cinc-cluster.name
  associate_public_ip_address = true
  enable_monitoring           = true
  user_data                   = <<GRRR
Content-Type: multipart/mixed; boundary="==BOUNDARY=="
MIME-Version: 1.0

--==BOUNDARY==
MIME-Version: 1.0
Content-Type: text/text/x-shellscript; charset="us-ascii"
#!/bin/bash -v
cat >> /home/ubuntu/.ssh/authorized_keys <<- EOF
${var.adminkeys}
EOF

--==BOUNDARY==
MIME-Version: 1.0
Content-Type: text/text/x-shellscript; charset="us-ascii"
#!/bin/bash -v
add-apt-repository -y ppa:keithw/mosh
apt-get update
apt-get -y upgrade
apt-get -y install mosh fail2ban awscli jq
systemctl start fail2ban
systemctl enable fail2ban
cat > /etc/fail2ban/jail.local <<- EOF
[sshd]
enabled = true
port = 22
filter = sshd
logpath = /var/log/auth.log
maxretry = 3
EOF
systemctl restart fail2ban

--==BOUNDARY==
MIME-Version: 1.0
Content-Type: text/text/x-shellscript; charset="us-ascii"
#!/bin/bash -v

curl -s -L https://s3.amazonaws.com//aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -o /tmp/awslogs-agent-setup.py
python /tmp/awslogs-agent-setup.py --non-interactive --region ${var.aws_region}

mkdir -p /etc/awslogs
mkdir -p /var/lib/awslogs

cat > /etc/awslogs/awscli.conf <<- EOF
[default]
region = ${var.aws_region}
[plugins]
cwlogs = cwlogs
EOF

cat > /etc/awslogs/awslogs.conf <<- EOF
[general]
state_file = /var/lib/awslogs/agent-state
[/var/log/messages]
datetime_format = %b %d %H:%M:%S
file = /var/log/messages
log_stream_name = {instance_id}
log_group_name = ${var.servicename}-${var.environment}-system
[/var/log/secure]
datetime_format = %b %d %H:%M:%S
file = /var/log/secure
log_stream_name = {instance_id}
log_group_name = ${var.servicename}-${var.environment}-system
[/var/log/cron]
datetime_format = %b %d %H:%M:%S
file = /var/log/cron
log_stream_name = {instance_id}
log_group_name = ${var.servicename}-${var.environment}-system
[/var/log/cloud-init.log]
datetime_format = %b %d %H:%M:%S
file = /var/log/cloud-init.log
log_stream_name = {instance_id}
log_group_name = ${var.servicename}-${var.environment}-system
[/var/log/cfn-init.log]
datetime_format = %Y-%m-%d %H:%M:%S
file = /var/log/cfn-init.log
log_stream_name = {instance_id}
log_group_name = ${var.servicename}-${var.environment}-system
[/var/log/cfn-init-cmd.log]
datetime_format = %Y-%m-%d %H:%M:%S
file = /var/log/cfn-init-cmd.log
log_stream_name = {instance_id}
log_group_name = ${var.servicename}-${var.environment}-system
[/var/log/dmesg]
file = /var/log/dmesg
log_stream_name = {instance_id}
log_group_name = ${var.servicename}-${var.environment}-system
[/var/log/aws-signing-proxy/proxy.log]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/aws-signing-proxy/proxy.log
log_stream_name = {instance_id}
[/var/log/opscode/bookshelf/current]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/opscode/bookshelf/current
log_stream_name = {instance_id}
[/var/log/opscode/oc_bifrost/current]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/opscode/oc_bifrost/current
log_stream_name = {instance_id}
[/var/log/opscode/opscode-erchef/current]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/opscode/opscode-erchef/current
log_stream_name = {instance_id}
[/var/log/opscode/opscode-erchef/requests.log]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/opscode/opscode-erchef/requests.log.*
log_stream_name = {instance_id}
[/var/log/opscode/opscode-pushy-server/current]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/opscode/opscode-pushy-server/current
log_stream_name = {instance_id}
[/var/log/opscode/redis_lb/current]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/opscode/redis_lb/current
log_stream_name = {instance_id}
[/var/log/opscode/oc_id/current]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/opscode/oc_id/current
log_stream_name = {instance_id}
[/var/log/opscode/nginx/access.log]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/opscode/nginx/access.log
log_stream_name = {instance_id}
[/var/log/opscode/nginx/error.log]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/opscode/nginx/error.log
log_stream_name = {instance_id}
[/var/log/chef-manage/redis/current]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/chef-manage/redis/current
log_stream_name = {instance_id}
[/var/log/chef-manage/web/current]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/chef-manage/web/current
log_stream_name = {instance_id}
[/var/log/chef-manage/worker/current]
log_group_name = ${var.servicename}-${var.environment}-service
file = /var/log/chef-manage/worker/current
log_stream_name = {instance_id}
EOF

systemctl start --no-block awslogs
systemctl enable awslogs

--==BOUNDARY==
MIME-Version: 1.0
Content-Type: text/text/x-shellscript; charset="us-ascii"
#!/bin/bash -v
set -o errexit -o nounset -o pipefail

echo ">>> Installing aws-signing-proxy command"
curl -s -L https://github.com/chef-customers/aws-signing-proxy/releases/download/v0.5.0/aws-signing-proxy -o /usr/local/bin/aws-signing-proxy
chmod 755 /usr/local/bin/aws-signing-proxy

cat > /etc/systemd/system/aws-signing-proxy.service <<- EOF
[Unit]
Description=aws-signing-proxy
After=network.target
#
[Service]
ExecStart=/usr/local/bin/aws-signing-proxy
Restart=always
#
[Install]
WantedBy=multi-user.target
EOF

cat > /etc/aws-signing-proxy.yml <<- EOF
listen-address: 127.0.0.1
port: 9200
target: https://${module.es.endpoint}
region: ${var.aws_region}
no-file-log: false
stdout-log: true
EOF

systemctl daemon-reload && systemctl start aws-signing-proxy

--==BOUNDARY==
#!/bin/bash -v

mkdir -p /etc/opscode
mkdir -p /etc/opscode-push-jobs-server
mkdir -p /etc/chef-manage

cat > /etc/opscode-push-jobs-server/opscode-push-jobs-server.rb <<- EOF
opscode_pushy_server['vip'] = '127.0.0.1'
opscode_pushy_server['server_name_advertised'] = '${aws_route53_record.pushjobs.fqdn}'.downcase
EOF

cat > /etc/chef-manage/manage.rb <<- EOF
disable_sign_up = true
EOF

cat > /etc/opscode/chef-server.rb <<- EOF
api_fqdn = '${var.servicename}.${var.dns_zone}'.downcase
nginx['enable_non_ssl'] = true
postgresql['external'] = true
postgresql['vip'] = '${module.pg-master.this_db_instance_address}'
postgresql['port'] = ${module.pg-master.this_db_instance_port}
postgresql['db_superuser'] = 'chefadmin'
postgresql['db_superuser_password'] = 'YourPwdShouldBeLongAndSecure34129381293182938124777'
oc_chef_authz['http_init_count'] = 100
oc_chef_authz['http_queue_max'] = 200
opscode_erchef['authz_pooler_timeout'] = 2000
oc_bifrost['db_pool_init'] = 10
oc_bifrost['db_pool_max'] = 20
oc_bifrost['db_pool_queue_max'] = 40
opscode_erchef['depsolver_worker_count'] = 4
opscode_erchef['depsolver_timeout'] = 20000
opscode_erchef['db_pool_init'] = 10
opscode_erchef['db_pool_max'] = 20
opscode_erchef['db_pool_queue_max'] = 40
opscode_erchef['keygen_cache_workers'] = 2
opscode_erchef['keygen_cache_size'] = 100
opscode_erchef['nginx_bookshelf_caching'] = :on
opscode_erchef['s3_url_expiry_window_size'] = '100%'
opscode_erchef['s3_parallel_ops_fanout'] = 10
opscode_erchef['search_provider'] = 'elasticsearch'
opscode_erchef['search_queue_mode'] = 'batch'
opscode_solr4['external'] = true
opscode_solr4['external_url'] = 'http://localhost:9200'
opscode_solr4['elasticsearch_shard_count'] = ${var.es_instance_count}
opscode_solr4['elasticsearch_replica_count'] = ${var.es_replica_count}
bookshelf['storage_type'] = :sql
bookshelf['db_pool_init'] = 10
bookshelf['db_pool_max'] = 20
bookshelf['vip'] = '${var.servicename}.${var.dns_zone}'.downcase
rabbitmq['enable'] = false
rabbitmq['management_enabled'] = false
rabbitmq['queue_length_monitor_enabled'] = false
opscode_expander['enable'] = false
dark_launch['actions'] = false
default_orgname '${var.defaultorgname}'.empty? ? null : '${var.defaultorgname}'
EOF

--==BOUNDARY==
MIME-Version: 1.0
Content-Type: text/text/x-shellscript; charset="us-ascii"
#!/bin/bash -v

cd /tmp/
echo ">>> Downloading and install package"
curl -s -o installer.deb -OL ${var.core_server_installer_url}
dpkg -i installer.deb

export STACKNAME="${var.servicename}-${var.environment}"
export BUCKET="${aws_s3_bucket.cinc-cluster-state.id}"
export AWS_REGION=${var.aws_region}
export PUBLIC_URL="https://${var.servicename}.${var.dns_zone}" #needed for the server reconfigure to work

aws s3 cp s3://$BUCKET/$STACKNAME-startup.sh startup.sh
chmod +x startup.sh
./startup.sh
GRRR

  lifecycle {
    create_before_destroy = true
  }
}
