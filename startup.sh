#!/bin/bash -x
# top level shell script used to support cinc startup in a clustered mode
# this shell script tries to make the correct guess about bootstraping via the node
# or just initialising a non-bootstrap type node
# based on startup.sh of from @ https://github.com/chef-customers/aws_native_chef_server/

echo "[INFO] startup.sh : Startup Checks.."

# Provided variables that are required: STACKNAME, BUCKET, AWS_REGION
test -n "${STACKNAME}" || export SOMETHINGWRONG="YES"
test -n "${BUCKET}" || export SOMETHINGWRONG="YES"
test -n "${AWS_REGION}" || export SOMETHINGWRONG="YES"

# Check for several utilities we depend on
test -f `which cinc-server-ctl` || export SOMETHINGWRONG="YES"
test -f `which aws` || export SOMETHINGWRONG="YES"

if [ $SOMETHINGWRONG -eq "YES" ] ; then
  echo "[INFO] startup.sh : Checks Failed"
  exit 1
else
  echo "[INFO] startup.sh : Checks Passed"
fi

# Determine if we are the bootstrap node
# BOOTSTRAP_TAGS will be empty if not
INSTANCE_ID=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
BOOTSTRAP_TAGS=`aws ec2 describe-tags --region $AWS_REGION --filter "Name=resource-id,Values=$INSTANCE_ID" --output=text | grep bootstrap`

# Check if the configs already exist in S3 - a sign that bootstrap already successfully happened once
CONFIGS_EXIST=`aws s3 ls s3://${BUCKET}/${STACKNAME}/ | grep migration-level`

function download_config () {
  aws s3 sync s3://${BUCKET}/${STACKNAME}/etc_opscode /etc/opscode --exclude "chef-server.rb"
  mkdir -p /var/opt/opscode/upgrades
  touch /var/opt/opscode/bootstrapped
  aws s3 cp s3://${BUCKET}/${STACKNAME}/migration-level /var/opt/opscode/upgrades/
}

function upload_config () {
  aws s3 sync /etc/opscode s3://${BUCKET}/${STACKNAME}/etc_opscode
  aws s3 cp /var/opt/opscode/upgrades/migration-level s3://${BUCKET}/${STACKNAME}/
}

function server_reconfigure () {
  cinc-server-ctl reconfigure --chef-license=accept
  # chef-manage-ctl reconfigure --accept-license
}

function server_upgrade () {
  cinc-server-ctl reconfigure --chef-license=accept
  cinc-server-ctl upgrade
  cinc-server-ctl start
  # chef-manage-ctl reconfigure --accept-license
}

function prevent_dns_overload {
  # erchef will constantly try to DNS lookup the bookshelf hostname, which is simply itself.
  # as a workaround, we're just going to put the hostname
  echo "`ifconfig eth0 | grep inet\ | awk '{print $2}'` `hostname`.local" >> /etc/hosts
}

function push_jobs_configure () {
	cinc-server-ctl reconfigure --accept-license
	# opscode-push-jobs-server-ctl reconfigure
	cinc-server-ctl restart
}

# Here we go
prevent_dns_overload

# If we're not bootstrap OR a config already exists, sync down the rest of the secrets first before reconfiguring
if [ -z "${BOOTSTRAP_TAGS}" ] || [ -n "${CONFIGS_EXIST}" ] ; then
  echo "[INFO] startup.sh : Configuring this node as a regular CINC API server or restoring a Bootstrap"
  download_config
else
  echo "[INFO] startup.sh : Configuring this node as a Bootstrap CINC API server"
fi

# Upgrade/Configure handler
# If we're a bootstrap and configs already existed, upgrade
if [ -n "${BOOTSTRAP_TAGS}" ] && [ -n "${CONFIGS_EXIST}" ] ; then
  echo "[INFO] startup.sh : Looks like we're on a boostrap node that may need to be upgraded"
  server_upgrade
else
  echo "[INFO] startup.sh : Running cinc-server-ctl reconfigure"
  server_reconfigure
fi

# the bootstrap instance should sync files after reconfigure, regardless if configs exist or not (upgrades)
if [ -n "${BOOTSTRAP_TAGS}" ]; then
  echo "[INFO] startup.sh : Configuring cinc-server and push-jobs"
  push_jobs_configure
  echo "[INFO] startup.sh : Syncing bootstrap secrets up to S3"
  upload_config
fi

  echo "[INFO] startup.sh : Finished"
