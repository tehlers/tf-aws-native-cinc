## Terraform code to setup an AWS-native CINC Server

# OBJECTIVES

This is a terraform codebase to run an AWS NATIVE deployment of the CINC server from https://cinc.sh/

Based heavily on the https://github.com/chef-customers/aws_native_chef_server/ aka ANCS repo.

This repo contains many terraform resources and other objects and scripts ported and cleaned up, from the ANCS repo.

ANCS's main.sh has been renamed startup.sh for clarity.

This repo aims to achieve the similar goals to those aimed at by the ANCS repo.

Key differences - this is implemented in terraform, not cloudformation.
There are no 'built' amis involved - so fewer steps, no packer.
We just use ubuntu images directly and drop a deb of the cinc-core.
Terraform apply to get the latest ubuntu 18.04.

In time we hope the CINC project produces more than just the core server. such as
a cinc pushjobs and a cinc management console. I will endeavour to add those here.

This repo aims to be somewhere between a 'module' and a 'top level repo'.

We aim for the cleanliness of a module but the power-user sense of a top level repo.

Fork it, duplicate it, and start making your own AWS-native cinc infra !

# MODULES

This codebase makes use of the following excellent modules:

* github.com/terraform-community-modules/tf_aws_elasticsearch [for elasticsearch]

* github.com/terraform-aws-modules/terraform-aws-rds [for postgres]

# HOW TO USE

Fork or Duplicate this repo and use it directly.

Or call it as a git-sourced terraform module repo with a tag (I'd suggest forking it just to stay safe)

Set your own variables per the example/cinc-infra.tf (a modular example) and variables.tf

Note these variables will need to be tuned to your specific requirements. Database and instance sizes are a factor.

```
$ terraform init
$ terraform plan
$ terraform apply
```

ssh onto the bootstrap node as ubuntu user and then create your cinc admin users and org

```
$ sudo cinc-server-ctl user-create bob Bob Salarybob bob@bobcorp.net REALLYEASILYGUESSEDPASSWORD --filename /home/ubuntu/bob.pem
$ sudo cinc-server-ctl org-create bobcorp "BobCorp" --association_user bob --filename /home/ubuntu/bobcorp-validator.pem
```

it's chefs all the way down from here.

# CONCEPTS TO UNDERSTAND

There are two scaling groups used to run this service.

1. the cinc-bootscrap scaling group - a chef server of 1 to initialize databases and provide a minimal service
2. the cinc-nodes scaling group - any number of servers you need to handle your load in addition to the bootstrap

both of these scaling groups run identical user-data but the startup.sh at the
end of the user-data will do different things based on the asg cluster member name

on the bootstrap server, if no config exists in s3, we generate the initial secrets (see startup.sh) and publish them

when applying the infra the cinc-nodes cluster depends on the cinc-bootstrap cluster :

```
aws_autoscaling_group.cinc-bootstrap: Still creating... [40s elapsed]
....
aws_autoscaling_group.cinc-bootstrap: Still creating... [1m10s elapsed]
aws_autoscaling_group.cinc-bootstrap: Creation complete after 1m19s [id=bootstrap-cinc-poc-XX]
aws_autoscaling_attachment.pushjobs: Creating...
aws_autoscaling_group.cinc-nodes: Creating...
aws_autoscaling_attachment.pushjobs: Creation complete after 6s [id=bootstrap-cinc-poc-XX]
aws_autoscaling_group.cinc-nodes: Still creating... [10s elapsed]
....
aws_autoscaling_group.cinc-nodes: Still creating... [50s elapsed]
aws_autoscaling_group.cinc-nodes: Creation complete after 53s [id=nodes-cinc-poc-XX]
```

This setup mirrors the ANCS setup but without the cloudformation readyness callbacks.
