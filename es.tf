module "es" {
  source      = "github.com/terraform-community-modules/tf_aws_elasticsearch?ref=v1.3.0"
  es_version  = 6.8
  domain_name = "${var.servicename}-${var.environment}"
  vpc_options = {
    security_group_ids = [aws_security_group.db.id]
    subnet_ids         = [aws_subnet.private_az1.id, aws_subnet.private_az2.id]
  }
  create_iam_service_linked_role = var.es_create_iam_service_linked_role
  instance_count                 = var.es_instance_count
  instance_type                  = var.es_instance_type
  dedicated_master_type          = var.es_dedicated_master_type
  es_zone_awareness              = true
  ebs_volume_size                = var.es_ebs_volume_size
  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}"
    },
  )
}

output "elasticsearch_endpoint" {
  value = module.es.endpoint
}
