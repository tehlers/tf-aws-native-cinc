# cpu load based
# scale up alarm
resource "aws_autoscaling_policy" "cpu-policy-scaleup" {
  name                   = "${var.servicename}-${var.environment}-cpu-load-scaleup"
  autoscaling_group_name = aws_autoscaling_group.cinc-nodes.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "1"
  cooldown               = "180"
  policy_type            = "SimpleScaling"
}

# scale down alarm
resource "aws_autoscaling_policy" "cpu-policy-scaledown" {
  name                   = "${var.servicename}-${var.environment}-cpu-load-scaledown"
  autoscaling_group_name = aws_autoscaling_group.cinc-nodes.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "-1"
  cooldown               = "360"
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "cpu-alarm-scaleup" {
  alarm_name          = "${var.servicename}-${var.environment}-cpu-alarm-scaleup"
  alarm_description   = "${var.servicename}-${var.environment}-cpu-alarm-scaleup"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "5"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "60"
  dimensions = {
    "AutoScalingGroupName" = aws_autoscaling_group.cinc-nodes.name
  }
  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.cpu-policy-scaleup.arn]
}


resource "aws_cloudwatch_metric_alarm" "cpu-alarm-scaledown" {
  alarm_name          = "${var.servicename}-${var.environment}-cpu-alarm-scaledown"
  alarm_description   = "${var.servicename}-${var.environment}-cpu-alarm-scaledown"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "10"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "40"
  dimensions = {
    "AutoScalingGroupName" = aws_autoscaling_group.cinc-nodes.name
  }
  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.cpu-policy-scaledown.arn]
}
