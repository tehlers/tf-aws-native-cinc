#
# Networking
#

# Create a VPC to launch our instances into
resource "aws_vpc" "default" {
  cidr_block = var.cidr
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-vpc"
    },
  )
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.default.id
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-internetgateway"
    },
  )
}

# Create an Elastic IP to use for the NAT gateway
resource "aws_eip" "nat_gw1" {
  vpc        = true
  depends_on = [aws_internet_gateway.default]
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-natgw1"
    },
  )
}

# Create a private subnet to launch our instances into (zone A)
resource "aws_subnet" "private_az1" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = cidrsubnet(var.cidr, 4, 0)
  availability_zone = "${var.aws_region}a"
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-private-az1"
    },
  )
}

# Create a private subnet to launch our instances into (zone B)
resource "aws_subnet" "private_az2" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = cidrsubnet(var.cidr, 4, 1)
  availability_zone = "${var.aws_region}b"
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-private-az2"
    },
  )
}

# Create a private subnet to launch our instances into (zone C)
resource "aws_subnet" "private_az3" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = cidrsubnet(var.cidr, 4, 2)
  availability_zone = "${var.aws_region}c"
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-private-az3"
    },
  )
}

# Create a public subnet to facilitate NAT (zone A)
resource "aws_subnet" "public_az1" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = cidrsubnet(var.cidr, 4, 3)
  availability_zone = "${var.aws_region}a"
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-public-az1"
    },
  )
}

# Create a public subnet to facilitate NAT (zone B)
resource "aws_subnet" "public_az2" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = cidrsubnet(var.cidr, 4, 4)
  availability_zone = "${var.aws_region}b"
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-public-az2"
    },
  )
}

# Create a public subnet to facilitate NAT (zone C)
resource "aws_subnet" "public_az3" {
  vpc_id            = aws_vpc.default.id
  cidr_block        = cidrsubnet(var.cidr, 4, 5)
  availability_zone = "${var.aws_region}c"
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-public-az3"
    },
  )
}

# NAT gateway to allow our EC2 instance behind private IPs to get outside (zone A)
resource "aws_nat_gateway" "private_az1" {
  allocation_id = aws_eip.nat_gw1.id
  subnet_id     = aws_subnet.public_az1.id
  depends_on    = [aws_internet_gateway.default]
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-az1"
    },
  )
}

# Internet route for public subnets
resource "aws_route_table" "internet" {
  vpc_id = aws_vpc.default.id
  route {
    cidr_block = var.anywhere
    gateway_id = aws_internet_gateway.default.id
  }
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}"
    },
  )
}

# NAT route for private subnets
resource "aws_route_table" "nat_az1" {
  vpc_id = aws_vpc.default.id
  route {
    cidr_block     = var.anywhere
    nat_gateway_id = aws_nat_gateway.private_az1.id
  }
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-nat1"
    },
  )
}

resource "aws_route_table_association" "private_az1" {
  subnet_id      = aws_subnet.private_az1.id
  route_table_id = aws_route_table.nat_az1.id
}

resource "aws_route_table_association" "private_az2" {
  subnet_id      = aws_subnet.private_az2.id
  route_table_id = aws_route_table.nat_az1.id
}

resource "aws_route_table_association" "private_az3" {
  subnet_id      = aws_subnet.private_az3.id
  route_table_id = aws_route_table.nat_az1.id
}

resource "aws_route_table_association" "public_az1" {
  subnet_id      = aws_subnet.public_az1.id
  route_table_id = aws_route_table.internet.id
}

resource "aws_route_table_association" "public_az2" {
  subnet_id      = aws_subnet.public_az2.id
  route_table_id = aws_route_table.internet.id
}

resource "aws_route_table_association" "public_az3" {
  subnet_id      = aws_subnet.public_az3.id
  route_table_id = aws_route_table.internet.id
}
