resource "aws_iam_policy" "cloudwatch" {
  name        = "${var.servicename}-${var.environment}-cloudwatch"
  path        = "/"
  description = "Allows ECS containers to send logs to CloudWatch"
  policy      = <<EOF
{
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents",
          "logs:DescribeLogStreams"
        ],
        "Resource" : [
          "arn:aws:logs:*:*:*"
        ]
      }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "cinc-cloudwatch" {
  role       = aws_iam_role.cinc-cluster.id
  policy_arn = aws_iam_policy.cloudwatch.arn
}

resource "aws_cloudwatch_log_group" "system" {
  name              = "${var.servicename}-${var.environment}-system"
  retention_in_days = 90
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-systemlogs"
    },
  )
}

resource "aws_cloudwatch_log_group" "service" {
  name              = "${var.servicename}-${var.environment}-service"
  retention_in_days = 90
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-servicelogs"
    },
  )
}
