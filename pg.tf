####################################
# Variables common to both instnaces
####################################
locals {
  pg_engine         = "postgres"
  pg_engine_version = "9.6.12"
  pg_port           = "5432"
}

###########
# Master DB
###########
module "pg-master" {
  source = "github.com/terraform-aws-modules/terraform-aws-rds?ref=v2.18.0"

  identifier = "${var.servicename}-${var.environment}-master"

  engine            = local.pg_engine
  engine_version    = local.pg_engine_version
  instance_class    = var.pg_instance_class
  allocated_storage = var.pg_allocated_storage

  name     = "${var.servicename}${var.environment}"
  username = var.pg_username
  password = var.pg_password
  port     = local.pg_port

  vpc_security_group_ids = [aws_security_group.db.id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # Backups are required in order to create a replica
  backup_retention_period = 1

  # DB subnet group
  subnet_ids = [aws_subnet.private_az2.id, aws_subnet.private_az2.id, aws_subnet.private_az3.id]

  create_db_option_group    = false
  create_db_parameter_group = false
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-master"
    },
  )
}

############
# Replica DB
############
module "pg-replica" {
  source = "github.com/terraform-aws-modules/terraform-aws-rds?ref=v2.18.0"

  identifier = "${var.servicename}-${var.environment}-replica"

  # Source database. For cross-region use this_db_instance_arn
  replicate_source_db = module.pg-master.this_db_instance_id

  engine            = local.pg_engine
  engine_version    = local.pg_engine_version
  instance_class    = var.pg_instance_class
  allocated_storage = var.pg_allocated_storage

  # Username and password must not be set for replicas
  username = ""
  password = ""
  port     = local.pg_port

  vpc_security_group_ids = [aws_security_group.db.id]

  maintenance_window = "Tue:00:00-Tue:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = 0

  # Not allowed to specify a subnet group for replicas in the same region
  create_db_subnet_group = false

  create_db_option_group    = false
  create_db_parameter_group = false
  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}-replica"
    },
  )
}


output "pg_host" {
  value = module.pg-master.this_db_instance_address
}


output "pg_port" {
  value = module.pg-master.this_db_instance_port
}
