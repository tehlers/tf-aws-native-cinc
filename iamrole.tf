resource "aws_iam_instance_profile" "cinc-cluster" {
  name = "${var.servicename}-${var.environment}-cluster"
  role = aws_iam_role.cinc-cluster.name
}

resource "aws_iam_role" "cinc-cluster" {
  name = "cinc-nodes"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "cinc-cluster" {
  name        = "${var.servicename}-${var.environment}-cluster-policy"
  description = "permissions to do logging, clustering, s3 listing etc"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*", "ec2:CreateSnapshot", "ec2:CreateTags", "ec2:DeleteSnapshot", "ec2:DescribeSnapshots", "ec2:DescribeVolumes",
        "s3:List*", "autoscaling:*", "cloudwatch:PutMetricData", "cloudwatch:GetMetricStatistics", "cloudwatch:ListMetrics"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Action": [
        "logs:PutLogEvents", "logs:CreateLogStream", "logs:CreateLogGroup"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:logs:${var.aws_region}:*:log-group:*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "cinc-nodes" {
  role       = aws_iam_role.cinc-cluster.name
  policy_arn = aws_iam_policy.cinc-cluster.arn
}
