
resource "aws_elb" "pushjobs" {
  name            = "${var.servicename}-${var.environment}-pushjobs"
  security_groups = [aws_security_group.elb.id]
  subnets         = [aws_subnet.public_az1.id, aws_subnet.public_az2.id, aws_subnet.public_az3.id]

  listener {
    instance_port     = 10000
    instance_protocol = "TCP"
    lb_port           = 10000
    lb_protocol       = "TCP"
  }

  listener {
    instance_port     = 10002
    instance_protocol = "TCP"
    lb_port           = 10002
    lb_protocol       = "TCP"
  }

  listener {
    instance_port     = 10003
    instance_protocol = "TCP"
    lb_port           = 10003
    lb_protocol       = "TCP"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:10002"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = merge(
    var.standard_tags,
    {
      Name = "${var.servicename}-${var.environment}"
    },
  )

}

resource "aws_lb_listener_rule" "pushjobs" {
  listener_arn = aws_lb_listener.cinc.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.cinc-bootstrap.arn
  }

  condition {
    path_pattern {
      values = ["/organizations/*/pushy/*"]
    }
  }
}

resource "aws_autoscaling_attachment" "pushjobs" {
  autoscaling_group_name = aws_autoscaling_group.cinc-bootstrap.id
  elb                    = aws_elb.pushjobs.id
}

resource "aws_route53_record" "pushjobs" {
  zone_id = join("", data.aws_route53_zone.selected.*.zone_id)
  name    = "${var.servicename}-pushjobs"
  type    = "CNAME"
  ttl     = 300
  records = [aws_elb.pushjobs.dns_name]
}

output "pushjobs_hostname" {
  value = aws_route53_record.pushjobs.fqdn
}
