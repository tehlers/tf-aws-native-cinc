terraform {
  backend "s3" {
    bucket = "CHANGEME-BUCKET-NAME"
    key    = "tf-aws-native-cinc-poc-state.tfstate"
    region = "eu-west-1"
  }
}

provider aws {
  region = var.aws_region
}

variable "standard_tags" {
  default = {
    "Owner" : "CINC Admins",
    "Billing" : "EXPENSE-ACCOUNT",
    "Service" : "cinc",
    "Environment" : "poc",
  }
  description = "Standard resource tag used for all resources that can take them"
  type        = map(string)
}

module "aws-native-cinc" {
  source = "gitlab.com/cinc-project/tf-aws-native-cinc"
  core_server_installer_url = "https://gitlab.com/cinc-project/distribution/server/-/jobs/753957293/artifacts/raw/data/ubuntu/18.04/cinc-server-core_14.0.48-1_amd64.deb"
  # this needs to be a valid deb, readable. Maybe put it in its own bucket ? the path in this example calls an "unstable" test build.
  aws_region = "eu-west-1"
  servicename = "cinc"
  environment = "poc"
  standard_tags = var.standard_tags
  ec2_instance_type = "t2.medium"
  cinc_nodes_max_size = 5
  cinc_nodes_min_size = 1
  cinc_nodes_desired_capacity = 1 # initial desired capacity - this will be ignored once the load is tracked
  pg_instance_class = "db.t3.small"
  pg_allocated_storage = 5
  es_instance_type = "t2.small.elasticsearch"
  es_dedicated_master_type = "t2.small.elasticsearch"
  es_instance_count = 2
  es_replica_count = 2
  es_create_iam_service_linked_role = true # or false if already done
  es_ebs_volume_size = 35
  defaultorgname = "poc"
  dns_zone = "poc.domain.com"
  ssh_key_name = "cinc_poc_admin"
  cidr = "192.168.0.0/24"
  cert_arn = "arn:aws:acm:eu-west-1:ACCOUNTID:certificate/CERTID"
  pg_username = "chefadmin"
  pg_password = "VERYLONGrandomPASSWORD9018249----qZusyfhbausdyfgwfysdf"
  ec2_pubkey = "SETME"
  adminkeys = "SETME"
  adminranges = ["0.0.0.0/0"]
}
