# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ---------------------------------------------------------------------------------------------------------------------

variable "core_server_installer_url" {
  default = "https://gitlab.com/cinc-project/distribution/server/-/jobs/753957293/artifacts/raw/data/ubuntu/18.04/cinc-server-core_14.0.48-1_amd64.deb"
}

variable "aws_region" {
  default = "eu-west-1"
}

variable "servicename" {
  default = "cinc"
}

variable "environment" {
  default = "poc"
}

variable "standard_tags" {
  default = {
    "Owner" : "DevOps",
    "Service" : "cinc",
    "Env" : "poc",
  }
  description = "Standard resource tag used for all resources that can take them"
  type        = map(string)
}

variable "ec2_instance_type" {
  default = "t2.medium"
}

variable "cinc_nodes_max_size" {
  default = 10
}

variable "cinc_nodes_min_size" {
  default = 1
}

variable "cinc_nodes_desired_capacity" {
  default     = 1
  description = "initial desired capacity - this will be ignored once the load is tracked"
}

variable "pg_instance_class" {
  default = "db.t3.small"
}

variable "pg_allocated_storage" {
  default = 5
}

variable "es_instance_type" {
  default = "t2.small.elasticsearch"
}

variable "es_dedicated_master_type" {
  default = "t2.small.elasticsearch"
}

variable "es_instance_count" {
  default = 2
}

variable "es_replica_count" {
  default = 2
}

variable "es_create_iam_service_linked_role" {
  default = true
}

variable "es_ebs_volume_size" {
  default = 35
}

variable "defaultorgname" {
  default = "poc"
}

variable "anywhere" {
  description = "CIDR range of anywhere"
  default     = "0.0.0.0/0"
}

variable "dns_zone" {
  default = "poc.mydomain.io"
}

variable "ssh_key_name" {
  default = "admin_rsa"
}

variable "cidr" {
  default = "172.26.198.0/24"
}

variable "cert_arn" {
  description = "AWS ARN to the certificate to be associated to the ALB to enable HTTPS connections"
  default     = "arn:aws:acm:eu-west-1:XX:certificate/YY"
}

variable "pg_username" {
  default = "chefadmin"
}

variable "pg_password" {
  default = "SETME"
}

variable "ec2_pubkey" {
  description = "ssh keypair"
  default     = "ssh-rsa SETME keypair"
}

variable "adminkeys" {
  description = "additional keypairs"
  default = "ssh-rsa SETMETOO admin1@bob.org"
}

variable "adminranges" {
  description = "admin ranges for ssh access etc"
  default = ["0.0.0.0/0"]
}
