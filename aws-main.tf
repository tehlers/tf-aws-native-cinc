terraform {
  required_version = ">= 0.12"
}

# Look up the Route53 Hosted Zone by domain name
data "aws_route53_zone" "selected" {
  name = "${var.dns_zone}."
}
